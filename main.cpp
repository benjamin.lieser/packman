#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <fstream>
#include <iostream>
#include <cstring>
#include "Board.h"

using namespace std;

int main()
{
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(800, 800), "Packman", sf::Style::Default, settings);

    ifstream level("levels/default.txt");
    if(!level) {
        cerr << "Error while loading the levelfile: " << strerror(errno) << endl;
        return 1;
    }
    auto g = Board::file2grid(level);
    Board board(g);


    sf::Clock clock;
    sf::Time now = clock.getElapsedTime();
    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event{};
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (event.type == sf::Event::Resized) {
                // update the view to the new size of the window
                sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
                window.setView(sf::View(visibleArea));
            }

            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Right) {
                    board.playerRight();
                }
                if (event.key.code == sf::Keyboard::Left) {
                    board.playerLeft();
                }
                if (event.key.code == sf::Keyboard::Down) {
                    board.playerDown();
                }
                if (event.key.code == sf::Keyboard::Up) {
                    board.playerUp();
                }
                if (event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape) {
                    window.close();
                }
                if(event.key.code == sf::Keyboard::R) {
                    board.~Board();
                    new(&board) Board(g);
                }
            }
        }
        sf::Time diff = clock.getElapsedTime() - now;
        now = clock.getElapsedTime();
        if(board.update(diff.asSeconds())) {
            window.close();
        }
        board.render(window);
        int wait = max(0,10 - diff.asMilliseconds());
        this_thread::sleep_for(chrono::milliseconds(wait));
    }
    return 0;
}