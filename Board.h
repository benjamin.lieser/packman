//
// Created by benjamin on 23.07.18.
//

#ifndef PACKMAN_BOARD_H
#define PACKMAN_BOARD_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include "Entity.h"

/**
 * This class represtens the internal state of the board, like the position of the player and ghosts.
 * The borad is represented by a grid. One cell could be a wall, or a way. The positions of player and ghosts are floatings
 *
 * It offers a render function, to render the state to a window
 */

enum class FieldType {Wall, Empty, Point, PlayerStart, GhostStart, PowerPoint};
enum class GameState {Running, Won, Loos, End};


class Board {
private:
    int height; /**The hight of the borad. (Number of rows of the grid) */
    int width; /**The width of the board. (Number of cols of the grid) */
    std::vector<std::vector<FieldType>> grid;
    std::vector<Entity> entities;
    int remainingPoints;
    GameState state=GameState::Running;
    float powered = 0;
    constexpr static float powerTime = 4.0;


public:
    explicit Board(std::vector<std::vector<FieldType>> grid);
    ~Board();
    void render(sf::RenderWindow& window);
    bool update(float time);

    bool isWall(unsigned int x, unsigned int y) const;

    bool isValidPosition(float x, float y, float xWidth, float yWidth) const;

    void playerRight();
    void playerLeft();
    void playerUp();
    void playerDown();

    static std::vector<std::vector<FieldType>> file2grid(std::ifstream& file);
};


#endif //PACKMAN_BOARD_H
