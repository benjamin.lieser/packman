//
// Created by benjamin on 25.07.18.
//

#ifndef PACKMAN_ENTITY_H
#define PACKMAN_ENTITY_H

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

class Board;

enum class Direction {Left,Right,Up,Down,Nothing};
enum class EntityType {Player, Ghost};


class Entity {
private:
    float x;
    float y;
    const Board *board;
    float movement = 10; //Movement per unit of time
    Direction akkDirection;
    Direction nextDirection;
    bool isDirectionValid(Direction d, float movement) const;
public:
    float inactive = 0.0;
    bool collision(Entity& e) const;
    EntityType type;
    sf::Color color;
    Direction getDirection() const;
    float xWidth;
    float yWidth;
    Entity(const Board *board, sf::Sprite *sprite, float x, float y, float xWidth, float yWidth, EntityType type, float movement);
    sf::Sprite *sprite;
    float getX() const;
    float getY() const;
    void updateDirection(Direction d);
    void setAkkDirection(Direction d);
    bool updatePosition(float time);


    static void updateCoo(Direction d, float movement, float &x, float &y);
    static bool isOpposite(Direction a, Direction b);
    static const std::vector<sf::Color> colors;
};


#endif //PACKMAN_ENTITY_H
