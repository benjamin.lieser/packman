//
// Created by benjamin on 23.07.18.
//

#include "Board.h"
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <algorithm>

void Board::render(sf::RenderWindow &window) {
    auto size = window.getSize();
    int pixelWidth = size.x;
    int pixelHeight = size.y;
    int pixelPerUnitW = pixelWidth/width;
    int pixelPerUnitH = pixelHeight/height;
    sf::RectangleShape wall(sf::Vector2f(pixelPerUnitW, pixelPerUnitH));
    //Clearing the window
    window.clear(sf::Color::Black);

    //Rendering the Maze
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
            if(grid[x][y] == FieldType::Wall) { //Its a wall
                wall.setFillColor(sf::Color::Blue);
            } else { //Its not a wall
                wall.setFillColor(sf::Color::Black);
            }
            wall.setPosition(x*pixelPerUnitW,y*pixelPerUnitH);
            window.draw(wall);
        }
    }
    sf::CircleShape point(0.1*pixelPerUnitH);
    point.setFillColor(sf::Color::Yellow);

    sf::CircleShape Spoint(0.15*pixelPerUnitH);
    Spoint.setFillColor(sf::Color::White);

    //Rendering Points
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
            if(grid[x][y] == FieldType::Point) {
                point.setPosition(x*pixelPerUnitW + pixelPerUnitW/2 - (0.1*pixelPerUnitH)/2, y*pixelPerUnitH +pixelPerUnitW/2 - (0.1*pixelPerUnitH)/2);
                window.draw(point);
            }
            if(grid[x][y] == FieldType::PowerPoint) {
                Spoint.setPosition(x*pixelPerUnitW + pixelPerUnitW/2 - (0.15*pixelPerUnitH)/2, y*pixelPerUnitH +pixelPerUnitW/2 - (0.15*pixelPerUnitH)/2);
                window.draw(Spoint);
            }
        }
    }

    //Rendering the entities
    for(int i = 0; i < entities.size(); i++) {
        Entity& e = entities[i];
        if(e.inactive > 0) {
            continue;
        }
        auto box = e.sprite->getLocalBounds();
        e.sprite->setScale((float)pixelPerUnitW/box.width * e.xWidth, (float)pixelPerUnitH/box.height *e.yWidth);
        e.sprite->setPosition(e.getX()*pixelPerUnitW + pixelPerUnitW/2, e.getY()*pixelPerUnitH + pixelPerUnitH/2);

        e.sprite->setOrigin(box.width/2, box.height/2);
        switch (e.getDirection()) {
            case(Direction::Up):
                e.sprite->setRotation(270);
                break;
            case(Direction::Down):
                e.sprite->setRotation(90);
                break;
            case(Direction::Left):
                e.sprite->setRotation(180);
                break;
            case(Direction::Right):
                e.sprite->setRotation(0);
        }
        if(i > 0) {
            if(powered > 0) {
                e.sprite->setColor(sf::Color::Blue);
            } else {
                e.sprite->setColor(e.color);
            }

        }
        window.draw(*e.sprite);
    }

    //render Text
    if(state==GameState::Won) {
        sf::Font font;
        if (!font.loadFromFile("./fonts/font.ttf")) {
            std::cerr << "Could not load font" << std::endl;
        }
        sf::Text text;
        text.setFont(font);
        text.setString("Du hast gewonnen");
        float textWidth = text.getLocalBounds().width;
        text.setPosition(pixelWidth/2 - textWidth/2, 0);

        window.draw(text);
    }
    if(state==GameState::Loos) {
        sf::Font font;
        if (!font.loadFromFile("./fonts/font.ttf")) {
            std::cerr << "Could not load font" << std::endl;
        }
        sf::Text text;
        text.setFont(font);
        text.setString("Du hast verloren :(");
        float textWidth = text.getLocalBounds().width;
        text.setPosition(pixelWidth/2 - textWidth/2, 0);
        window.draw(text);
    }

    window.display();
}

Board::Board(std::vector<std::vector<FieldType>> grid) : height((int)grid[0].size()), width((int)grid.size()), grid(grid), state(GameState::Running) {
    auto pacmanT = new sf::Texture();
    pacmanT->loadFromFile("./images/pacman.png");
    pacmanT->setSmooth(true);
    auto pacmanS = new sf::Sprite();
    pacmanS->setTexture(*pacmanT);

    auto ghostT = new sf::Texture();
    ghostT->loadFromFile("./images/ghost.png");
    ghostT->setSmooth(true);
    auto ghostS = new sf::Sprite();
    ghostS->setTexture(*ghostT);
    remainingPoints = 0;
    int color = 0;
    for(int x = 0; x < grid.size(); x++) {
        for(int y = 0; y < grid[0].size(); y++) {
            switch(grid[x][y]) {
                case FieldType::Point:
                    remainingPoints++;
                    break;
                case FieldType::PlayerStart:
                    entities.emplace_back(this, pacmanS, x, y, 0.9, 0.9, EntityType::Player, 8);
                    break;
                case FieldType::GhostStart:
                    entities.emplace_back(this, ghostS, x, y, 0.9, 0.9, EntityType::Ghost, 6);
                    (--entities.end())->color = Entity::colors[color++];

            }
        }
    }
}

std::vector<std::vector<FieldType>> Board::file2grid(std::ifstream &file) {
    std::vector<std::vector<FieldType>> grid;
    for(std::string line; getline(file, line);) {
        std::vector<FieldType> row;
        for(char cell : line) {
            switch(cell) {
                case '*':
                    row.push_back(FieldType::Wall);
                    break;
                case 'P':
                    row.push_back(FieldType::PlayerStart);
                    break;
                case 'G':
                    row.push_back(FieldType::GhostStart);
                    break;
                case '+':
                    row.push_back(FieldType::PowerPoint);
                    break;
                default:
                    row.push_back(FieldType::Point);
            }
        }
        grid.push_back(row);
    }
    std::vector<std::vector<FieldType>> gridT(grid[0].size(), std::vector<FieldType>(grid.size(), FieldType::Empty));

    for(unsigned int i = 0; i < grid[0].size(); i++) {
        for(unsigned int j = 0; j < grid.size(); j++) {
            gridT.at(i).at(j) = grid.at(j).at(i);
        }
    }
    return gridT;
}

bool Board::isWall(unsigned int x, unsigned int y) const {
    if(x >= width || y >= height || x < 0 || y < 0) {
        return true;
    }
    return (grid.at(x).at(y) == FieldType::Wall);
}

bool Board::isValidPosition(float x, float y, float xWidth, float yWidth) const {
    return !(isWall(x, y) || isWall(x + xWidth, y + yWidth) || isWall(x, y + yWidth) || isWall(x + xWidth, y));
}

void Board::playerRight() {
    entities[0].updateDirection(Direction::Right);
}

void Board::playerLeft() {
    entities[0].updateDirection(Direction::Left);
}

void Board::playerUp() {
    entities[0].updateDirection(Direction::Up);
}

void Board::playerDown() {
    entities[0].updateDirection(Direction::Down);
}

Board::~Board() {
    delete entities[0].sprite->getTexture();
    delete entities[0].sprite;

    delete entities[1].sprite->getTexture();
    delete entities[1].sprite;
}

bool Board::update(float time) {
    if(state == GameState::Loos || state ==GameState::Won) {
        return false;
    }
    if(powered > 0) {
        powered = std::max((float)0.0, powered-time);
    }
    for(Entity &e: entities) {
        if(e.inactive > 0) {
            e.inactive = std::max((float)0.0, e.inactive - time);
        }
        if(!e.updatePosition(time) && e.type == EntityType::Ghost) {
            e.setAkkDirection(static_cast<Direction>(rand()%4));
        }
        if(e.type == EntityType::Ghost && (rand()%10 == 0)) {
            Direction newD = static_cast<Direction>(rand()%4);
            if(!Entity::isOpposite(e.getDirection(), newD)) {
                e.updateDirection(newD);
            }
        }
    }
    if(grid.at(entities[0].getX()).at(entities[0].getY()) == FieldType::Point) {
        grid.at(entities[0].getX()).at(entities[0].getY()) = FieldType::Empty;
        remainingPoints--;
    }
    if(grid.at(entities[0].getX()).at(entities[0].getY()) == FieldType::PowerPoint) {
        grid.at(entities[0].getX()).at(entities[0].getY()) = FieldType::Empty;
        powered=powerTime;
    }
    if(remainingPoints == 0) {
        state=GameState::Won;
    }
    for(Entity& e: entities) {
        if(e.type==EntityType::Ghost && e.inactive==0.0 && e.collision(entities[0])) {
            if(powered > 0) {
                e.inactive=4.0;
            } else {
                state = GameState::Loos;
            }
        }
    }
    return false;
}
