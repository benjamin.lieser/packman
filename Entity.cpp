//
// Created by benjamin on 25.07.18.
//

#include "Entity.h"
#include "Board.h"
#include <cmath>
#include <iostream>

const std::vector<sf::Color> Entity::colors = std::vector<sf::Color> {sf::Color(255,0,0), sf::Color(66,134,244), sf::Color(229,244,66), sf::Color(66,244,69), sf::Color(244,179,66)};

Entity::Entity(const Board *board, sf::Sprite *t, float x, float y, float xWidth, float yWidth, EntityType type, float movement)
        : board(board), sprite(t), x(x), y(y), xWidth(xWidth), yWidth(yWidth), akkDirection(Direction::Nothing), nextDirection(Direction::Nothing),
          type(type), movement(movement) {}

float Entity::getX() const {
    return x;
}

float Entity::getY() const {
    return y;
}

bool Entity::updatePosition(float time) {
    float diff = movement*time;
    if(akkDirection == nextDirection || nextDirection==Direction::Nothing) {
        if(isDirectionValid(akkDirection, diff)) {
            updateCoo(akkDirection, diff, x, y); //Do the actual movement
            return !(akkDirection==Direction::Nothing);
        }
    } else {
        if(isDirectionValid(nextDirection, 0.5) || !isDirectionValid(akkDirection, diff)) {
            if(isDirectionValid(nextDirection, diff)) {
                updateCoo(nextDirection, diff, x, y);
                akkDirection = nextDirection;
                nextDirection = Direction::Nothing;
                return !(nextDirection==Direction::Nothing);
            }
            akkDirection = nextDirection;
            nextDirection = Direction::Nothing;
            return false;
        } else {
            if(isDirectionValid(akkDirection, diff)) {
                updateCoo(akkDirection, diff, x, y);
                return !(akkDirection==Direction::Nothing);
            }
        }
    }
    return false;
}

bool Entity::isDirectionValid(Direction d, float mov) const {
    float newX = x;
    float newY = y;
    updateCoo(d,mov,newX,newY);
    return board->isValidPosition(newX, newY, xWidth, yWidth);
}

void Entity::updateCoo(Direction d, float mov, float &x, float &y) {
    switch (d) {
        case(Direction::Right):
            x+=mov;
            break;
        case(Direction::Left):
            x-=mov;
            break;
        case(Direction::Up):
            y-=mov;
            break;
        case(Direction::Down):
            y+=mov;
            break;
        //Do Nothing
    }
}

void Entity::updateDirection(Direction d) {
    nextDirection = d;
}

Direction Entity::getDirection() const {
    return akkDirection;
}

void Entity::setAkkDirection(Direction d) {
    akkDirection = d;
}

bool Entity::isOpposite(Direction a, Direction b) {
    switch(a) {
        case Direction::Right:
            return b == Direction::Left;
        case Direction::Left:
            return b == Direction::Right;
        case Direction::Up:
            return b == Direction::Down;
        case Direction::Down:
            return b == Direction::Up;
    }
    return false;
}

bool Entity::collision(Entity &e) const {
    sf::Rect<float> r(x, y, xWidth, yWidth);
    if(r.contains(e.x, e.y) || r.contains(e.x, e.y + e.yWidth) || r.contains(e.x + e.xWidth, e.y) || r.contains(e.x + e.xWidth, e.y + e.yWidth)) {
        return true;
    }
    return false;
}
